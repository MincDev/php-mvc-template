<?php
    /**
     * This file handles all paths for the framework. Not to be amended.
     * 
     * Minc Development
     * Copyright (c) Minc Development (Pty) Ltd. (https://mincdevelopment.co.za)
     *
     * Licensed under The MIT License
     * Redistributions of files must retain the above copyright notice.
     *
     * @author        Christopher Smit <christopher@mincdevelopment.co.za>
     * @copyright     Minc Development (Pty) Ltd. (https://mincdevelopment.co.za)
     * @version       1.0.0
     * @license       MIT License (https://opensource.org/licenses/mit-license.php)
     */

    /**
     * The protocol for the server
     */
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";

    /**
     *  Directory separator
     */
    define("DS", DIRECTORY_SEPARATOR);

    // define("LOCAL_PATH", "http://$_SERVER[HTTP_HOST]".dirname(dirname($_SERVER['PHP_SELF'])) . DS);
    // define("SERVER_PATH", "/");

    /*
     * The full path to the directory which holds "src"
     */
    define("ROOT", "{$protocol}$_SERVER[HTTP_HOST]".dirname(dirname($_SERVER['PHP_SELF'])) . DS);
    // define('ROOT', Config::SYSTEM_UAT_MODE ? LOCAL_PATH : SERVER_PATH);

    /**
     * This is the root of the project as an absolute path
     */
    define('ABSOLUTE_ROOT', dirname(__DIR__) . "/");

    /*
     * The actual directory name for the application directory.
     */
    define('APP_DIR', "App");

    /*
    * Path to the application's directory.
    */
    define('APP', ROOT . APP_DIR . DS);

    /**
     * View path
     */
    define('VIEW_PATH', APP . "Views" . DS);

    /*
    * Path to the bin directory.
    */
    define('BIN', ROOT . 'Bin' . DS);

    /*
    * File path to the webroot directory.
    */
    define('WWW_ROOT', 'webroot' . DS);

    /*
    * File path to the assets directory.
    */
    define('ASSETS', WWW_ROOT . "assets" . DS);

    /**
     * Upload path
     */
    define("UPLOAD_PATH", WWW_ROOT . "assets/uploads" . DS);

    /*
    * Path to the tests directory.
    */
    define('TESTS', ROOT . 'tests' . DS);

    /*
    * Path to the temporary files directory.
    */
    define('TMP', ROOT . 'tmp' . DS);

    /*
    * Path to the logs directory.
    */
    define('LOGS', ABSOLUTE_ROOT . 'logs' . DS);

    /*
    * Base file to use for assets
    */
    define('ASSET_BASE', ROOT);