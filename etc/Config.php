<?php

namespace etc;

/**
 * The core configuration file for the framework. 
 * 
 * Minc Development
 * Copyright (c) Minc Development (Pty) Ltd. (https://mincdevelopment.co.za)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Christopher Smit <christopher@mincdevelopment.co.za>
 * @copyright     Minc Development (Pty) Ltd. (https://mincdevelopment.co.za)
 * @version       1.0.0
 * @license       MIT License (https://opensource.org/licenses/mit-license.php)
 */
class Config
{
    /**
     * Show or hide error messages on screen
     * @var boolean
     */
    const SHOW_ERRORS = false;

    /**
     * Specifies whether the system is in UAT mode. 
     * Useful if you need to delegate specific actions based on live or dev
     * @var boolean
     */
    public const SYSTEM_UAT_MODE = true;

    /**
     * Specifies whether the system should use CSRF.
     * Not implemented currently
     * @var boolean
     */
    public const CSRF_ENABLED = false;

    /**
     * The name of the CSRF token
     * @var string
     */
    public const CSRF_TOKEN_NAME = "_csrfToken";

    /**
     * If enabled, the framework will attempt to automatically bind uri's to 
     * Controllers and Actions if no routes are found
     */
    public const AUTO_BINDING_ENABLED = true;

    /**
     * Database connection configurations
     * @var string
     */
    public const UAT_DB_HOST        = "";
    public const UAT_DB_USER        = "";
    public const UAT_DB_PASS        = "";
    public const UAT_DB_SCHEMA      = "";

    public const LIVE_DB_HOST        = "";
    public const LIVE_DB_USER        = "";
    public const LIVE_DB_PASS        = "";
    public const LIVE_DB_SCHEMA      = "";

    public const DATABASE_HOST      = Self::SYSTEM_UAT_MODE ? Self::UAT_DB_HOST    : Self::LIVE_DB_HOST;
    public const DATABASE_USER      = Self::SYSTEM_UAT_MODE ? Self::UAT_DB_USER    : Self::LIVE_DB_USER;
    public const DATABASE_PASSWORD  = Self::SYSTEM_UAT_MODE ? Self::UAT_DB_PASS    : Self::LIVE_DB_PASS;
    public const DATABASE_SCHEMA    = Self::SYSTEM_UAT_MODE ? Self::UAT_DB_SCHEMA  : Self::LIVE_DB_SCHEMA;
}
