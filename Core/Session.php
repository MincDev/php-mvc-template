<?php

namespace Core;

/**
 * This class manages sessions throughout the framework and can be accessed through the core controller
 * 
 * Minc Development
 * Copyright (c) Minc Development (Pty) Ltd. (https://mincdevelopment.co.za)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Christopher Smit <christopher@mincdevelopment.co.za>
 * @copyright     Minc Development (Pty) Ltd. (https://mincdevelopment.co.za)
 * @version       1.0.0
 * @license       MIT License (https://opensource.org/licenses/mit-license.php)
 */
class Session {

    public const SESSKEY_LOGGED_USER    = 'isLogged';
    public const SESSKEY_TIMEOUT        = 'sTimeout';
    public const SESSKEY_LAST_ACTIVITY  = 'lActive';

    /**
     * Constructor, checks whether a session exists, and starts one if it does not
     */
    public function __construct() 
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

    /**
     * Used for testing. Can echo the contents of a session.
     *
     * @return void
     */
    public function echoSession() {
        echo '<pre>';
        print_r($_SESSION);
        echo '</pre>';
        exit();
    }

    /**
     * This function will create an entire session based on an array passed to it. 
     * You can specify multiple session key value pairs at once
     *
     * @param array|null $sessionParams The PHP Array which contains the key value pairs
     * @return void
     */
    public function setSession(?array $sessionParams = null): void 
    {
        if ($sessionParams <> null) {
            foreach ($sessionParams as $key => $value) {
                $_SESSION[$key] = $value;
            }
        }
    }

    /**
     * This function will set a single session key value pair
     *
     * @param string $key The key for this session variable
     * @param object $value The value for this session variable
     * @return void
     */
    public function setObject(string $key, object $value): void
    {
        if (isset($_SESSION[$key]) && is_array($_SESSION[$key])) {
            foreach($value as $innerKey => $innerValue) {
                $_SESSION[$key][$innerKey] = $innerValue;
            }
        } else {
            $_SESSION[$key] = $value;
        }
    }

    /**
     * This function will append to an existing session key value pair
     *
     * @param string $key The key for this session variable
     * @param object $value The value for this session variable
     * @return void
     */
    public function appendObject(string $key, object $value): void 
    {
        $_SESSION[$key][] = $value;
    }

    /**
     * This function will retrieve a session key's value. 
     *
     * @param string $key The key of the value you want to retrieve. To retrieve inner key values, separate by => eg: Key=>InnerKey
     * @return object|null
     */
    public function getObject(string $key): ?object
    {
        if (strpos("x".$key, '=>') !== false) {
            $result = $_SESSION;
            $keys = explode("=>", $key);

            // For loop to get result by keys
            for ($i = 0; $i < count($keys); $i++) {
                $result = isset($result[$keys[$i]]) ? $result[$keys[$i]] : null;
            }
            return $result;
        } else {
            return isset($_SESSION[$key]) ? $_SESSION[$key] : null;
        }
    }

    /**
     * This function will check if a session key exists
     *
     * @param string $key The key you want to check.
     * @return boolean
     */
    public function objectExists(string $key): bool
    {
        return isset($_SESSION[$key]) ? true : false;
    }

    /**
     * This function will delete a session key value pair
     *
     * @param string $key The key of the object you want to delete
     * @return void
     */
    public function removeObject($key): void
    {
        unset($_SESSION[$key]);
    }

    /**
     * This function will validate whether a user is currently logged in
     *
     * @return boolean
     */
    public function validSession(): bool 
    {
        return (isset($_SESSION[Self::SESSKEY_LOGGED_USER]) && $_SESSION[Self::SESSKEY_LOGGED_USER]);
    }

    /**
     * This function will close the current session
     * 
     * @return boolean
     */
    public function closeSession(): bool 
    {
        return session_destroy();
    }

    /**
     * This function will set a session timeout to expire the session after a period of time.
     * 
     * @param integer $seconds The number of seconds to keep the session alive
     * @return void
     */
    public function setTimeout(int $seconds) : void
    {
        $_SESSION[Self::SESSKEY_TIMEOUT] = $seconds;
        $_SESSION[Self::SESSKEY_LAST_ACTIVITY] = time();
    }

    /**
     * This function will check if the current session has expired and update if not.
     * 
     * @return boolean
     */
    public function sessionExpired(): bool 
    {
        $expireTime = $_SESSION[Self::SESSKEY_TIMEOUT];
        if ($_SESSION[Self::SESSKEY_LAST_ACTIVITY] < (time() - $expireTime)) {
            $_SESSION[Self::SESSKEY_LOGGED_USER] = false;
            return true;
        } else {
            $_SESSION[Self::SESSKEY_LAST_ACTIVITY] = time();
            return false;
        }
    }

    /**
     * This function will return the GUID created for the session by the browser
     * 
     * @return string
     */
    public function browserSession(): string
    {
        return \session_id();
    }
}