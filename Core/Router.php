<?php

namespace Core;

use Exception;
use Core\Security\CSRF;

/**
 * This is the framework router. It's responsible for routing all 
 * requests to the appropriate controllers and actions.
 * 
 * Minc Development
 * Copyright (c) Minc Development (Pty) Ltd. (https://mincdevelopment.co.za)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Christopher Smit <christopher@mincdevelopment.co.za>
 * @copyright     Minc Development (Pty) Ltd. (https://mincdevelopment.co.za)
 * @version       1.0.0
 * @license       MIT License (https://opensource.org/licenses/mit-license.php)
 */
class Router
{

    /**
     * Associative array of routes (the routing table)
     * @var array
     */
    protected $routes = [];

    /**
     * Parameters from the matched route
     * @var array
     */
    protected $params = [];

    /**
     * Parameters from the autobinded routes
     * @var array
     */
    protected $auto_params = [];

    /**
     * Parameters in the body
     * @var array
     */
    protected $request_body = [];

    /**
     * Referencee to the CSRF class
     *
     * @var CSRF
     */
    protected $csrf = null;

    /**
     * Add a route to the routing table
     *
     * @param string $route  The route URL
     * @param array  $params Parameters (controller, action, etc.)
     *
     * @return void
     */
    public function add($route, $params = [])
    {
        // Convert the route to a regular expression: escape forward slashes
        $route = preg_replace('/\//', '\\/', $route);

        // Convert variables e.g. {controller}
        $route = preg_replace('/\{([a-z]+)\}/', '(?P<\1>[a-z-]+)', $route);

        // Convert variables with custom regular expressions e.g. {id:\d+}
        $route = preg_replace('/\{([a-z]+):([^\}]+)\}/', '(?P<\1>\2)', $route);

        // Add start and end delimiters, and case insensitive flag
        $route = '/^' . $route . '$/i';

        $this->routes[$route] = $params;
    }

    /**
     * Get all the routes from the routing table
     *
     * @return array
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * Match the route to the routes in the routing table, setting the $params
     * property if a route is found.
     *
     * @param string $url The route URL
     *
     * @return boolean  true if a match found, false otherwise
     */
    public function match($url)
    {
        foreach ($this->routes as $route => $params) {
            if (preg_match($route, $url, $matches)) {
                // Get named capture group values
                foreach ($matches as $key => $match) {
                    if (is_string($key)) {
                        $params[$key] = $match;
                    }
                }

                $this->params = $params;
                return true;
            }
        }

        // If enabled in config 
        // Attempt to automatically bind if route does not exist
        if (\etc\Config::AUTO_BINDING_ENABLED) {
            $route_exploded = explode("/", $url);
            $controller = isset($route_exploded[0]) ? $route_exploded[0] : "";
            $action = isset($route_exploded[1]) ? $route_exploded[1] : "index";
            
            // Get the parameters if any
            unset($route_exploded[0]);
            unset($route_exploded[1]);
            $this->auto_params = array_values($route_exploded);

            if (empty($controller)) {
                // Nothing passed
                throw new Exception("No initial route found.");
            }

            if (strpos($action, "_") !== False) {
                $action = $this->convertToCamelCase(str_replace("_", " ", $action));
            }

            $this->add($url, ['controller' => $controller, 'action' => $action]);
            return $this->match($url);
        }

        return false;
    }

    /**
     * Get the currently matched parameters
     *
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Get the currently matched autobinded parameters
     *
     * @return array
     */
    public function getAutoParams()
    {
        return $this->auto_params;
    }

    /**
     * Get the body parameters
     *
     * @return array
     */
    public function getRequestBody()
    {
        return $this->request_body;
    }

    /**
     * Dispatch the route, creating the controller object and running the
     * action method
     *
     * @param string $url The route URL
     *
     * @return void
     */
    public function dispatch($url)
    {
        $this->csrf = new CSRF();

        // Exclude assest routes
        if ($this->isAssetRoute($url)) {
            return;
        }

        $this->extractQueryParams($url);
        $url = $this->removeQueryStringVariables($url);
        $requestBody = isset($_POST) ? $_POST : [];
        $requestBody['files'] = $this->extractFiles();
        $queryParams = $this->getParams();
        $requestWith = isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? $_SERVER['HTTP_X_REQUESTED_WITH'] : '';

        // Verify the CSRF token
        if (\etc\Config::CSRF_ENABLED) {
            $this->verifyCSRF();
        }

        if ($this->match($url)) {
            $controller = $this->params['controller'] . "Controller";
            $controller = $this->convertToStudlyCaps($controller);
            $controller = $this->getNamespace() . $controller;

            if (class_exists($controller)) {
                $controller_object = new $controller();
                $controller_object->setRequestBody($requestBody);
                $controller_object->setQueryParams(array_merge($queryParams, $this->getAutoParams()));
                $controller_object->setRequestWith($requestWith);

                $action = $this->convertToCamelCase(isset($this->params['action']) && 
                                                            !empty($this->params['action']) ? 
                                                            $this->params['action'] : "index");

                if (preg_match('/action$/i', $action) == 0) {
                    $controller_object->setActive($controller, $action);
                    if ($controller_object->before() !== false) {
                        $controller_object->$action();
                        $controller_object->after();
                    }
                } else {
                    throw new \Exception("Method '$action' in controller '$controller' cannot be called directly - remove the Action suffix to call this method", 500);
                }
            } else {
                throw new \Exception("Controller class '$controller' not found", 404);
            }
        } else {
            if (\etc\Config::AUTO_BINDING_ENABLED) {
                throw new \Exception('No route matched, and automatic binding was not able to detect the controller and action.', 404);
            } else throw new \Exception('No route matched and automatic binding is not enabled.', 404);
        }
    }

    /**
     * Checks whether a route passed through dispatch is an asset url
     *
     * @param string $url The url to check
     * @return boolean
     */
    protected function isAssetRoute(string $url): bool 
    {
        if (strpos("x".$url, "assets") !== False) {
            return true;
        } else return false;
    }

    /**
     * Convert the string with hyphens to StudlyCaps,
     * e.g. post-authors => PostAuthors
     *
     * @param string $string The string to convert
     *
     * @return string
     */
    protected function convertToStudlyCaps($string)
    {
        return str_replace(' ', '', ucwords(str_replace('-', ' ', $string)));
    }

    /**
     * Convert the string with hyphens to camelCase,
     * e.g. add-new => addNew
     *
     * @param string $string The string to convert
     *
     * @return string
     */
    protected function convertToCamelCase($string)
    {
        return lcfirst($this->convertToStudlyCaps($string));
    }

    /**
     * Remove the query string variables from the URL (if any). As the full
     * query string is used for the route, any variables at the end will need
     * to be removed before the route is matched to the routing table. For
     * example:
     *
     *   URL                           $_SERVER['QUERY_STRING']  Route
     *   -------------------------------------------------------------------
     *   localhost                     ''                        ''
     *   localhost/?                   ''                        ''
     *   localhost/?page=1             page=1                    ''
     *   localhost/posts?page=1        posts&page=1              posts
     *   localhost/posts/index         posts/index               posts/index
     *   localhost/posts/index?page=1  posts/index&page=1        posts/index
     *
     * A URL of the format localhost/?page (one variable name, no value) won't
     * work however. (NB. The .htaccess file converts the first ? to a & when
     * it's passed through to the $_SERVER variable).
     *
     * @param string $url The full URL
     *
     * @return string The URL with the query string variables removed
     */
    protected function removeQueryStringVariables($url)
    {
        if ($url != '') {
            $parts = explode('&', $url, 2);

            if (strpos($parts[0], '=') === false) {
                $url = $parts[0];
            } else {
                $url = '';
            }
        }

        return $url;
    }

    /**
     * Extracts the query parameters from the url.
     *
     * @param string $url The url to extract params from
     * @return void
     */
    protected function extractQueryParams(string $url): void
    {
        if ($url != '') {
            $parts = explode('&', $url, 2);

            if (isset($parts[1])) {
                if (strpos($parts[1], '=') !== false) {
                    $key_values = explode('&', $parts[1]);

                    foreach ($key_values as $key_value) {
                        $key_value_exp = explode('=', $key_value);
                        $this->params[$key_value_exp[0]] = $key_value_exp[1];
                    }
                }
            }
        }
    }

    /**
     * Extracts files from the request and formats the array.
     *
     * @return array
     */
    protected function extractFiles(): array 
    {
        $files = isset($_FILES) ? $_FILES : [];
        $files2 = [];
        foreach ($files as $input => $infoArr) {
            $filesByInput = [];
            foreach ($infoArr as $key => $valueArr) {
                if (is_array($valueArr)) { // file input "multiple"
                    foreach($valueArr as $i=>$value) {
                        $filesByInput[$input][$i][$key] = $value;
                    }
                } else { // -> string, normal file input
                    $filesByInput[$input] = $infoArr;
                    break;
                }
            }
            $files2 = array_merge($files2,$filesByInput);
        }
        // $files3 = [];
        // foreach($files2 as $file) { // let's filter empty & errors
        //     if (!$file['error']) $files3[] = $file;
        // }
        return $files2;
    }

    /**
     * Get the namespace for the controller class. The namespace defined in the
     * route parameters is added if present.
     *
     * @return string The request URL
     */
    protected function getNamespace()
    {
        $namespace = 'App\Controllers\\';

        if (array_key_exists('namespace', $this->params)) {
            $namespace .= $this->params['namespace'] . '\\';
        }

        return $namespace;
    }

    /**
     * Verifies the current CSRF token.
     *
     * @return boolean
     * @throws \Exception
     */
    protected function verifyCSRF(): bool
    {
        $X_CSRFToken = isset($_SERVER['HTTP_X_CSRFTOKEN']) ? $_SERVER['HTTP_X_CSRFTOKEN'] : '';

        if ($_SERVER['REQUEST_METHOD'] == "POST" || $_SERVER['REQUEST_METHOD'] == "PUT" || $_SERVER['REQUEST_METHOD'] == "DELETE") {
            if (!empty($X_CSRFToken)) {
                if ($this->csrf->verify($X_CSRFToken)) {
                    return true;
                } else throw new \Exception("CSRF Token Mismatch!");
            } else throw new \Exception("No CSRF token provided and CSRF is enabled!");
        } return true;
    }
}