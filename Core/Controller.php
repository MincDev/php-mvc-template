<?php

namespace Core;

use \Core\Session;
use \Core\Cookie;

/**
 * This is the core controller class for the MVC framework
 * 
 * Minc Development
 * Copyright (c) Minc Development (Pty) Ltd. (https://mincdevelopment.co.za)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Christopher Smit <christopher@mincdevelopment.co.za>
 * @copyright     Minc Development (Pty) Ltd. (https://mincdevelopment.co.za)
 * @version       1.0.0
 * @license       MIT License (https://opensource.org/licenses/mit-license.php)
 */
abstract class Controller
{
    /** 
     * The active controller
     * @var string
     */
    protected ?string $controller = null;

    /**
     * The active action
     * @var string
     */
    protected ?string $action = null;

    /**
     * Parameters from the matched route
     * @var array
     */
    protected $queryParams = [];

    /**
     * Parameters passed by POST
     * @var array
     */
    protected $requestBody = [];

    /**
     * The current session object
     * @var Session
     */
    protected $session;

    /**
     * Type of the call to the backend
     * @var string
     */
    protected string $requestWith = "";

    /**
     * Cookie reference
     * @var Cookie
     */
    private Cookie $cookie;

    /**
     * Class constructor
     *
     * @return void
     */
    public function __construct()
    {
        $this->session = new Session();
        $this->cookie = new Cookie();
    }

    /**
     * Get's the current PHP session
     * 
     * @return Session
     */
    public function getSession(): Session
    {
        return $this->session;
    }

    /**
     * Get's the context of cookies
     * 
     * @return Cookie
     */
    protected function getCookies(): Cookie
    {
        return $this->cookie;
    }

    /**
     * Sets the request body for the controller
     * 
     * @param array $requestBody The body to be set
     * 
     * @return void
     */
    public function setRequestBody($requestBody): void 
    {
        $this->requestBody = $requestBody;
    }

    /**
     * @deprecated This is replaced by getRequest()
     * 
     * Gets the request body for the controller
     * 
     * @return array
     */
    public function getRequestBody(): array
    {
        return $this->requestBody;
    }

    /**
     * Sets the query paramaters for the controller
     * 
     * @param array $queryParams The query parameters to be set
     * 
     * @return void
     */
    public function setQueryParams($queryParams): void 
    {
        $this->queryParams = $queryParams;
    }

    /**
     * @deprecated This is replaced by getRequest()
     * 
     * Gets the query params for the controller
     * 
     * @return array
     */
    public function getQueryParams(): array
    {
        return $this->queryParams;
    }

    /**
     * Returns the request data
     *
     * @return array
     */
    public function getRequest(): array 
    {
        return array_merge($this->queryParams, $this->requestBody);
    }

    /**
     * Sets the type of call being made
     *
     * @param string $requestWith The type of the call
     * @return void
     */
    protected function setRequestWith(string $requestWith): void 
    {
       $this->requestWith = $requestWith;
    }

    /**
     * Sets the active controller and action
     *
     * @param string $controller
     * @param string $action
     * @return void
     */
    protected function setActive(string $controller, string $action): void 
    {
        $controller = explode("\\", $controller);
        $this->controller = end($controller);
        $this->action = $action;
    }
    
    /**
     * Magic method called when a non-existent or inaccessible method is
     * called on an object of this class. Used to execute before and after
     * filter methods on action methods. Action methods need to be named
     * with an "Action" suffix, e.g. indexAction, showAction etc.
     *
     * @param string $name  Method name
     * @param array $args Arguments passed to the method
     *
     * @return void
     */
    public function __call($name, $args): void
    {
        $method = $name;

        if (method_exists($this, $method)) {
            call_user_func_array([$this, $method], $args);
        } else {
            throw new \Exception("Method $method not found in controller " . get_class($this), 404);
        }
    }

    /**
     * Returns a json string back to the view to be interpreted.
     * 
     * @param bool $successful Whether to return a successful response or a failed response
     * @param array $payload Additional parameters to return with the response
     * 
     * @return void
     */
    public function respond(bool $successful, array $payload = []): void 
    {
        $response["success"] = $successful;
        foreach ($payload as $key => $value) {
            $response[$key] = $value;
        }
        echo \json_encode($response);
        exit();
    }

    /**
     * Creates a custom json response to send to frontend
     *
     * @param array $payload The payload to be sent
     * @return void
     */
    public function rawRespond(array $payload): void 
    {
        foreach ($payload as $key => $value) {
            $response[$key] = $value;
        }
        echo \json_encode($response);
        exit();
    }

    /**
     * Before filter - called before an action method.
     *
     * @return void
     */
    protected function before(): void
    {
    }

    /**
     * After filter - called after an action method.
     *
     * @return void
     */
    protected function after(): void
    {
    }

    /**
     * Allows redirection from one controller function to another
     *
     * @param array $location The location to redirect to
     * @return void
     */
    protected function redirect(string $location): void
    {
        header("Location: " . ROOT . $location);
        exit();
    }

    /**
     * Allows the controller to render a view
     *
     * @param string $view The path to the view to be rendered, relative to the View folder
     * @param array $args The args to be passed to the view.
     * @return void
     */
    protected function render(string $view, array $args = []) {
        View::render($view, $args);
    }
}
