<?php

namespace Core;

/**
 * This is the core entity class. All entities must extend this class
 * 
 * Minc Development
 * Copyright (c) Minc Development (Pty) Ltd. (https://mincdevelopment.co.za)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Christopher Smit <christopher@mincdevelopment.co.za>
 * @copyright     Minc Development (Pty) Ltd. (https://mincdevelopment.co.za)
 * @version       1.0.0
 * @license       MIT License (https://opensource.org/licenses/mit-license.php)
 */

class Entity
{
    /**
     * Takes an mysql table array and assigns the values to the entity object
     *
     * @param array $entityParameters table array from mysql
     * @throws \ReflectionException
     */
    public function __construct(array $entityParameters = [])
    {
        $rc = new \ReflectionClass($this);
        foreach ($entityParameters as $key => $value) {
            $camelCaseName = lcfirst(str_replace(' ', '', \ucwords(str_replace("_", " ", $key))));

            if (property_exists($this, $camelCaseName)) {
                $property = $rc->getProperty($camelCaseName);
                $pType = $property->getType()->getName();
                if ($pType == "DateTime") {
                    call_user_func_array([$this, "set" . \ucwords(str_replace("_", "", $key))], [is_string($value) ? new \DateTime($value) : $value]);
                } else call_user_func_array([$this, "set" . \ucwords(str_replace("_", "", $key))], [$value]);
            }
        }
    }

    /**
     * Converts the entity object to an array
     *
     * @return array
     */
    public function toArray(): array 
    {
        $rc = new \ReflectionClass($this);
        $array = [];
        foreach ($rc->getProperties() as $property) {
            $ptype = $property->getType()->getName();
            $pName = $property->getName();
            if ($this->isPropertyInitialized($rc, $pName, $this)) {
                $snake_case = strtolower(preg_replace('/(?<=\d)(?=[A-Za-z])|(?<=[A-Za-z])(?=\d)|(?<=[a-z])(?=[A-Z])/', "_", $pName));
                
                // if ($ptype == "DateTime") {
                // $value = call_user_func([$this, "get".$property->getName()])->format("Y-m-d H:i:s");
                // } else 
                $value = call_user_func([$this, "get".$property->getName()]);
                $array[$snake_case] = $value;
            }
        }
        return $array;
    }

    /**
     * Checks whether the entity object is empty or contains at least one assigned property
     *
     * @return boolean
     */
    public function isEmpty(): bool 
    {
        return count((array) $this) == 0;
    }

    /**
     * Checks whether a ReflectionClass property is initialized.
     *
     * @param \ReflectionClass $rc The initialized ReflectionClass
     * @param string $propertyName The property name in this object
     * @param Object $object The object which contains this property
     * @return boolean
     */
    private function isPropertyInitialized(\ReflectionClass $rc, string $propertyName, Object $object): bool
    {
        $property = $rc->getProperty($camelCaseName = lcfirst(str_replace(' ', '', \ucwords(str_replace("_", " ", $propertyName)))));
        $property->setAccessible(true);
        return $property->isInitialized($object);
    }


    /**
     * Checks whether a property allows nulls
     *
     * @param string $propertyName The property name to check
     * @return boolean
     */
    public function isNullable(string $propertyName): bool 
    {
        $rc = new \ReflectionClass($this);
        $property = $rc->getProperty($propertyName);
        $property->setAccessible(true);
        return $property->getType()->allowsNull();
    }
}
