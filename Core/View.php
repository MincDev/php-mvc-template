<?php

namespace Core;

/**
 * The core view class for the framework. Responsible for rendering the views
 * 
 * Minc Development
 * Copyright (c) Minc Development (Pty) Ltd. (https://mincdevelopment.co.za)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Christopher Smit <christopher@mincdevelopment.co.za>
 * @copyright     Minc Development (Pty) Ltd. (https://mincdevelopment.co.za)
 * @version       1.0.0
 * @license       MIT License (https://opensource.org/licenses/mit-license.php)
 */
class View
{
    private static $global_args = [];

    /**
     * Render a view file - Accepts either PHP or Twig files
     *
     * @param string $view  The view file (Either twig or php file)
     * @param array $args  Associative array of data to display in the view (optional)
     *
     * @return void
     */
    public static function render($view, $args = [])
    {
        switch (pathinfo($view, PATHINFO_EXTENSION)) {
            case "php":
                extract(array_merge($args, Self::$global_args), EXTR_SKIP);

                $file = dirname(__DIR__) . "/App/Views/$view";  // relative to Core directory
        
                if (is_readable($file)) {
                    require $file;
                } else {
                    throw new \Exception("$file not found");
                }
                break;
            case "twig":
                static $twig = null;

                if ($twig === null) {
                    $loader = new \Twig\Loader\FilesystemLoader(dirname(__DIR__) . '/App/Views');
                    $twig = new \Twig\Environment($loader, ["debug" => \etc\Config::SYSTEM_UAT_MODE]);
                    $twig->addExtension(new \Twig\Extension\DebugExtension());
                }
        
                echo $twig->render($view, array_merge($args, Self::$global_args));
                break;
            default:
                throw new \Exception("Unsupported templating system.");
        }
    }

    /**
     * Sets additional parameters to a view
     *
     * @param array $args  Associative array of data to display in the view
     *
     * @return void
     */
    public static function setArgs(array $args): void
    {
        Self::$global_args = $args;
    }
}
