<?php

namespace Core\Common;

/**
 * A soft extension to the php array. This allows for more array functionality
 * 
 * Minc Development
 * Copyright (c) Minc Development (Pty) Ltd. (https://mincdevelopment.co.za)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Christopher Smit <christopher@mincdevelopment.co.za>
 * @copyright     Minc Development (Pty) Ltd. (https://mincdevelopment.co.za)
 * @version       1.0.0
 * @license       MIT License (https://opensource.org/licenses/mit-license.php)
 */
class _Array
{
    /**
     * Splits an array into sets of different lists
     *
     * @param array $input_array The array to split
     * @param integer $size The number of output arrays
     * @param ?boolean $preserve_keys Whether to preserve keys
     * @return array
     */
    public static function split_array(array &$input_array, int $size, ?bool $preserve_keys = null): array
    {
        $nr = (int)ceil(count($input_array) / $size);

        if ($nr > 0) {
            return array_chunk($input_array, $nr, $preserve_keys);
        }
    }
}