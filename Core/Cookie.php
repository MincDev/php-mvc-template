<?php

namespace Core;

/**
 * This class manages cookies for the framework and can be accessed throught the core controller
 * 
 * Minc Development
 * Copyright (c) Minc Development (Pty) Ltd. (https://mincdevelopment.co.za)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Christopher Smit <christopher@mincdevelopment.co.za>
 * @copyright     Minc Development (Pty) Ltd. (https://mincdevelopment.co.za)
 * @version       1.0.0
 * @license       MIT License (https://opensource.org/licenses/mit-license.php)
 */

class Cookie 
{
    /**
     * Constructor for the Cookie class. You can pass in a cookie array to immediately create a cookie.
     * 
     * @param array $cookie: Array containing cookie with the following parameters - name, value, options (options as per normal PHP seCookie)
     */
    public function __construct(array $cookie = []) 
    {
        if (!empty($cookie)) {
            $this->$cookie = $cookie;
            $this->set($cookie);
        }
    }

    /**
     * Formats a cookie value so a dot [.] is chaned to underscore [_]
     *
     * @param string $name The name to format
     * @return string
     */
    private function formatCookieName(string $name): string
    {
        return str_replace(".", "_", $name);
    }

    /**
     * Set a cookie
     * 
     * @param array $cookie: Array containing cookie with the following parameters - name, value, options (options as per normal PHP setCookie)
     * @return boolean
     */
    public  function set(array $cookie): bool 
    {
        return \setcookie($cookie["name"], $cookie["value"], isset($cookie["options"]) ? $cookie["options"] : []);
    }

    /**
     * Get a cookie
     * 
     * @param array $cookie: The name of the cookie to get
     */
    public  function get(string $name): string 
    {
        return (string) $_COOKIE[$this->formatCookieName($name)];
    }

    /**
     * Check if a cookie exists
     * 
     * @param string $name: The name of the cookie
     * 
     * @return bool
     */
    public  function exists(string $name): bool 
    {
        return (bool) isset($_COOKIE[$this->formatCookieName($name)]);
    }

    /**
     * Delete a cookie
     * 
     * @param string $name: The name of the cookie
     * 
     */
    public  function delete(string $name): void
    {
        \setcookie($this->formatCookieName($name), "", time() - 3600);
    }

    /**
     * Check if the browser allows cookies
     * 
     * @return bool
     */
    public  function cookies_allowed(): bool
    {
        \setcookie("cookie_check", "checked", time() + 3600, '/');

        if (count($_COOKIE) > 0) {
            $this->delete("cookie_check");
            return true;
        } else return false;
    }
}