<?php

namespace Core\Security;

use \Core\Cookie;

/**
 * This class handles CSRF functionality for the framework. CSRF can be enabled in config
 * 
 * Minc Development
 * Copyright (c) Minc Development (Pty) Ltd. (https://mincdevelopment.co.za)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Christopher Smit <christopher@mincdevelopment.co.za>
 * @copyright     Minc Development (Pty) Ltd. (https://mincdevelopment.co.za)
 * @version       1.0.0
 * @license       MIT License (https://opensource.org/licenses/mit-license.php)
 */
class CSRF 
{
    /**
     * The property to hold the CSRF token
     * @var string
     */
    private string $token; 

    private Cookie $cookie;

    /**
     * Constructor for the CSRF class. Checks whether a token exists and sets one if it does not
     */
    public function __construct() 
    {
        $this->cookie = new Cookie();
        // Check if a valid token exists
        if (!$this->cookie->exists(\etc\Config::CSRF_TOKEN_NAME)) {
            // no token exists, set a token.
            $this->generateToken();
        } else $this->token = $this->cookie->get(\etc\Config::CSRF_TOKEN_NAME);
    }

    /**
     * Function to generate a new CSRF token
     *
     * @return void
     */
    private function generateToken(): void
    {
        $this->token = \bin2hex(openssl_random_pseudo_bytes(32));
        $this->cookie->set([
            "name"      => \etc\Config::CSRF_TOKEN_NAME,
            "value"     => $this->token,
            "options"   => [
                'expires' => time() + 18000, 
                'path' => '/', 
                'samesite' => 'Lax'
            ]
        ]);
    }

    /**
     * Returns the current set CSRF token.
     *
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * Verifies whether the CSRF token is valid
     *
     * @param string $request_token The token retrieved from the request to verify against the server
     * @return boolean
     */
    public function verify(string $request_token): bool
    {
        if (!hash_equals($this->token, $request_token)) {
            // Possible malicious attempt. 
            // Do what is needed here
            return false;
        } else {
            // Reset the CSRF token as this one has now been used.
            $this->generateToken();
            return true;
        }
    }
}