<?php

namespace Core;

use Exception;
use MySql\Connector;

/**
 * The core model class. This class is responsible for making a connection to the database and 
 * can be used to save / update objects
 * 
 * Minc Development
 * Copyright (c) Minc Development (Pty) Ltd. (https://mincdevelopment.co.za)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Christopher Smit <christopher@mincdevelopment.co.za>
 * @copyright     Minc Development (Pty) Ltd. (https://mincdevelopment.co.za)
 * @version       1.0.0
 * @license       MIT License (https://opensource.org/licenses/mit-license.php)
 */
abstract class Model
{

    protected static Connector $conn;

    /**
     * Get the PDO database connection
     *
     * @return Connector
     */
    protected static function getConnection(): Connector
    {
        if (isset(Self::$conn)) {
            return Self::$conn;
        } else {
            Self::$conn = new Connector([
                "host"      => \etc\Config::DATABASE_HOST,
                "user"      => \etc\Config::DATABASE_USER,
                "passwd"    => \etc\Config::DATABASE_PASSWORD,
                "schema"    => \etc\Config::DATABASE_SCHEMA,
            ]);
            return Self::$conn;
        }
    }

    /**
     * Saves an object to the database. Uses reflection to identify which table to save to based on the object class name.
     *
     * @param Object $object The object to be saved to the corresponding database table
     * @return int
     */
    protected function saveObject(Object $object): int
    {
        $rc = new \ReflectionClass($object);
        $objectPath = explode('\\', get_class($object));
        $underscored = preg_replace('/\B([A-Z])/', '_$1', array_pop($objectPath));
        $objectName = strtolower($underscored);
        $conn = $this->getConnection();

        if ($conn->prepare("SHOW TABLES LIKE '$objectName'", null)->rowCount() > 0) {
            $strSql = "SELECT `COLUMN_NAME` 
                        FROM `INFORMATION_SCHEMA`.`COLUMNS` 
                        WHERE `TABLE_NAME`= '$objectName';";
            $tableStructure = $conn->prepare($strSql, null)->select();

            // If there is a primary key in this object, then this should be an update.
            $primaryKey = $conn->prepare("SHOW KEYS FROM $objectName WHERE Key_name = 'PRIMARY'", null)->select()['Column_name'];

            if ($this->isPropertyInitialized($rc, $this->convertToCamelCase($primaryKey), $object)) {
                // Update
                $strSql = "UPDATE $objectName SET ";
                foreach ($tableStructure as $column) {
                    $column = $column['COLUMN_NAME'];

                    if (!$this->isPropertyInitialized($rc, $this->convertToCamelCase($column), $object) || $column == $primaryKey) {
                        continue;
                    }

                    $strSql .= "$column = :$column,";

                    $property = $rc->getProperty($this->convertToCamelCase($column));
                    $pType = $property->getType()->getName();
                    
                    if ($pType == "DateTime") {
                        $dateTime = call_user_func([$object, "get".\ucwords(str_replace("_", "", $column))]);
                        $arrSql[":$column"] = isset($dateTime) ? $dateTime->format('Y-m-d H:i:s') : null;
                    } else $arrSql[":$column"] = call_user_func([$object, "get".\ucwords(str_replace("_", "", $column))]);
                }
                $strSql = rtrim($strSql, ",")." WHERE $primaryKey = :$primaryKey";
                $arrSql[":$primaryKey"] = call_user_func([$object, "get".\ucwords(str_replace("_", "", $primaryKey))]);

                if ($conn->prepare($strSql, $arrSql)->modify()) {
                    return call_user_func([$object, "get".\ucwords(str_replace("_", "", $primaryKey))]);;
                } else return null;
            } else {
                // Insert
                $strSql = "INSERT INTO $objectName (";
                foreach ($tableStructure as $column) {
                    $column = $column['COLUMN_NAME'];

                    if (!$this->isPropertyInitialized($rc, $this->convertToCamelCase($column), $object)) {
                        continue;
                    }

                    $strSql .= "$column,";
                }
                $strSql = rtrim($strSql, ",").") VALUES (";
                foreach ($tableStructure as $column) {
                    $column = $column['COLUMN_NAME'];

                    if (!$this->isPropertyInitialized($rc, $this->convertToCamelCase($column), $object)) {
                        continue;
                    }

                    $strSql .= ":$column,";

                    $property = $rc->getProperty($this->convertToCamelCase($column));
                    $pType = $property->getType()->getName();
                    
                    if ($pType == "DateTime") {
                        $dateTime = call_user_func([$object, "get".\ucwords(str_replace("_", "", $column))]);
                        $arrSql[":$column"] = $dateTime->format('Y-m-d H:i:s');
                    } else $arrSql[":$column"] = call_user_func([$object, "get".\ucwords(str_replace("_", "", $column))]);
                }
                $strSql = rtrim($strSql, ",").");";

                if ($conn->prepare($strSql, $arrSql)->modify()) {
                    return $conn->lastInsertId();
                } else return null;
            }
        } else throw new Exception("Table name '$objectName' does not exist in this database!", 404);
    }

    /**
     * Converts a string to camelCase.
     *
     * @param string $string The string to convert. E.g: person_id to personId
     * @return string
     */
    private function convertToCamelCase(string $string): string 
    {
        return lcfirst(str_replace(' ', '', \ucwords(str_replace("_", " ", $string))));
    }

    /**
     * Checks whether a ReflectionClass property is initialized.
     *
     * @param \ReflectionClass $rc The initialized ReflectionClass
     * @param string $propertyName The property name in this object
     * @param Object $object The object which contains this property
     * @return boolean
     */
    private function isPropertyInitialized(\ReflectionClass $rc, string $propertyName, Object $object): bool
    {
        $property = $rc->getProperty($this->convertToCamelCase($propertyName));
        $property->setAccessible(true);
        return $property->isInitialized($object);
    }
}
