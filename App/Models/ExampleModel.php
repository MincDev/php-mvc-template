<?php

namespace App\Models;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class ExampleModel extends \Core\Model
{

    /**
     * An example function showing how to get the database connection
     *
     * @return void
     */
    public function exampleFunction(): void 
    {
        $conn = parent::getConnection();
    }
}
