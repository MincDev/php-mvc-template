<?php

namespace App\Controllers;

/**
 * Home controller
 *
 * PHP version 7.0
 */
class ExampleController extends AppController
{
    /**
     * Show the index page
     *
     * @return void
     */
    public function index()
    {
        $this->render('Example/index.twig');
    }
}
