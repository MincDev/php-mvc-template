<?php

namespace App\Controllers;

use Core\Controller;

/**
 * This controller is to be used for extending all other controllers. 
 * It extends the core controller and can be used to override core controller functions.
 * 
 * Minc Development
 * Copyright (c) Minc Development (Pty) Ltd. (https://mincdevelopment.co.za)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Christopher Smit <christopher@mincdevelopment.co.za>
 * @copyright     Minc Development (Pty) Ltd. (https://mincdevelopment.co.za)
 * @version       1.0.0
 * @license       MIT License (https://opensource.org/licenses/mit-license.php)
 */
class AppController extends Controller 
{
    /**
     * Fires before an action is called on a controller.
     * Overrides the before function in the core controller
     *
     * @return void
     */
    protected function before(): void 
    {
        parent::before();
    }

    /**
     * Fires after an action is called on a controller.
     * Overrides the after function in the core controller
     *
     * @return void
     */
    protected function after(): void 
    {
        parent::after();
    }
}