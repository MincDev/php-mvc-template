#!/usr/bin/php -q
<?php

if (count($argv) == 1) {
    throw new Exception("No script provided to execute!");
}

$script = $argv[1];
$args = implode(" ", $argv);
$dir = rtrim($_SERVER['PHP_SELF'], "cmd.php");

echo shell_exec("php {$dir}/scripts/{$script}.php {$args} 2>&1");
