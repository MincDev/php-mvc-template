<?php

use etc\Config;
use MySql\Connector;

require_once dirname(dirname(dirname(__FILE__))).'/vendor/autoload.php';

new DBCompare();

class DBCompare 
{
    private ?PDO $pdo1 = null;
    private ?PDO $pdo2 = null;

    public function __construct()
    {
        $this->setDatabase1(Config::UAT_DB_HOST, Config::UAT_DB_SCHEMA, Config::UAT_DB_USER, Config::UAT_DB_PASS);
        $this->setDatabase2(Config::LIVE_DB_HOST, Config::LIVE_DB_SCHEMA, Config::LIVE_DB_USER, Config::LIVE_DB_PASS);
        $this->compare();
    }

    /**
     * Set database details for table 1
     *
     * @param string $host
     * @param string $schema
     * @param string $user
     * @param string $password
     */
    public function setDatabase1(string $host, string $schema, string $user, string $password)
    {
        $this->pdo1 = new \PDO("mysql:host=" . $host . ";dbname=" . $schema, $user, $password);
    }

    /**
     * Set database details for table 2
     *
     * @param string $host
     * @param string $schema
     * @param string $user
     * @param string $password
     */
    public function setDatabase2(string $host, string $schema, string $user, string $password)
    {
        $this->pdo2 = new \PDO("mysql:host=" . $host . ";dbname=" . $schema, $user, $password);
    }

    /**
     * Compare table
     *
     * @return void
     */
    public function compare(): void
    {

        $this->print("------------------TABLES--------------------", [], 2);


        $tables = $this->getTables(true);
        $tables2 = $this->getTables(false);

        $missingTables2 = $this->getMissingColumns($tables, $tables2);
        $missingTables1 = $this->getMissingColumns($tables2, $tables);

        if (count($missingTables1) > 0) {
            $this->print("Tables missing from UAT Database :", $missingTables1);
        }
        if (count($missingTables2) > 0) {
            $this->print("Tables missing from Live Database :", $missingTables2);
        }


        $this->print("----------------COLUMNS---------------------", [], 2);


        foreach ($tables as $table) {
            $columns1 = $this->getColumns($table, true);
            $columns2 = $this->getColumns($table, false);

            $missing1 = $this->getMissingColumns($columns1, $columns2);
            $missing2 = $this->getMissingColumns($columns2, $columns1);

            if (count($missing1) > 0) {
                $this->print("Columns missing from UAT Database - Table : $table", $missing1, 2);
            }
            if (count($missing2) > 0) {
                $this->print("Columns missing from Live Database - Table : $table", $missing2, 2);
            }
        }
    }

    /**
     * Get columns from table in a database
     *
     * @param string $tableName
     * @param bool $isTable1
     * @return array
     */
    public function getColumns(string $tableName, bool $isTable1): array
    {
        $stmt = $this->getConnection($isTable1)->prepare("select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME=:tablename");
        $columns = [];
        if ($stmt->execute([":tablename" => $tableName])) {
            $cols = $stmt->fetchAll(PDO::FETCH_ASSOC);
            foreach ($cols as $column) {
                $columns[] = $column["COLUMN_NAME"];
            }
        }
        return $columns;
    }

    /**
     * Get columns that are missing from column 2
     *
     * @param array|null $columns1
     * @param array|null $columns2
     * @return string[]
     */
    public function getMissingColumns(?array $columns1 = null, ?array $columns2 = null): array
    {
        $missingColumns = [];

        foreach ($columns1 as $column1) {
            $found = false;
            foreach ($columns2 as $column2) {
                if ($column1 === $column2) {
                    $found = true;
                }
            }
            if (!$found) {
                $missingColumns[] = $column1;
            }
        }
        return $missingColumns;
    }

    /**
     * Get tables from database
     *
     * @return string[]
     */
    private function getTables(bool $isTable1): array
    {
        $tables = [];
        $stmt = $this->getConnection($isTable1)->prepare("show tables");
        if ($stmt->execute()) {
            $results = $stmt->fetchAll();
            foreach ($results as $result) {
                $tables[] = $result[0];
            }
        }
        return $tables;
    }

    /**
     * Get connection
     *
     * @param bool $isTable1
     * @return PDO
     */
    public function getConnection(bool $isTable1): PDO
    {
        return $isTable1 ? $this->pdo1 : $this->pdo2;
    }

    /**
     * Prints a message to browser / command line
     *
     * @param string $message The message to print
     * @return void
     */
    private static function print(string $message, array $items = [], int $lineBreaks = 1): void 
    {
        $lbr = php_sapi_name() === 'cli' ? "\n" : "<br>";
        echo "{$message}";
        for ($i = 0; $i < count($items); $i++) {
            echo $lbr . $items[$i];
        }
        for ($i = 0; $i < $lineBreaks; $i++) {
            echo "{$lbr}";
        }
    }
}