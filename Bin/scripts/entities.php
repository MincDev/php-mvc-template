<?php

use MySql\Connector;

require_once dirname(dirname(dirname(__FILE__))).'/vendor/autoload.php';

$tableName = isset($argv[3]) ? $argv[3] : "";

new EntityBuilder($tableName);

class EntityBuilder 
{
    private Connector $conn;
    public function __construct(string $tableName)
    {
        $this->conn = new Connector([
            "host"      => \etc\Config::DATABASE_HOST,
            "user"      => \etc\Config::DATABASE_USER,
            "passwd"    => \etc\Config::DATABASE_PASSWORD,
            "schema"    => \etc\Config::DATABASE_SCHEMA,
        ]);

        $this->generateFiles($tableName);
    }


    private function generateFiles(string $tableName): void {
        try {
            if ($tableName == "") {
                $strSql = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA <> :TABLE_SCHEMA GROUP BY TABLE_NAME";
                $arrSql = [":TABLE_SCHEMA" => 'information_schema'];

                foreach ($this->conn->prepare($strSql, $arrSql)->select(true) as $table) {
                    $this->generateFile($table['TABLE_NAME']);
                }
            } else {
                $this->generateFile($tableName);
            }
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

    /**
     * Converts a string to camelCase.
     *
     * @param string $string The string to convert. E.g: person_id to personId
     * @return string
     */
    private function convertToCamelCase(string $string, bool $startLower = false): string 
    {
        if (!$startLower) {
            return ucfirst(str_replace(' ', '', \ucwords(str_replace("_", " ", $string))));
        } else {
            return lcfirst(str_replace(' ', '', \ucwords(str_replace("_", " ", $string))));
        }
    }

    private function generateFile(string $tableName): void 
    {
        echo "Creating entity for table name '{$tableName}'\n";

        $strSql = "SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = :TABLE_NAME";
        $arrSql = [":TABLE_NAME" => $tableName];

        $columns = [];
        $hasDateTime = false;
        foreach ($this->conn->prepare($strSql, $arrSql)->select(true) as $column) {
            $cInfo["primary_key"] = $column["COLUMN_KEY"] == "PRI";
            $cInfo["name"] = $column["COLUMN_NAME"];
            $cInfo["nullable"] = $column["IS_NULLABLE"] == "YES";

            if (isset($column["COLUMN_KEY"])) {
                $cInfo["primary_key"] = strcmp($column["COLUMN_KEY"], "PRI") == 0;
            } else {
                $cInfo["primary_key"] = strcmp($column["ORDINAL_POSITION"], "1") == 0;
            }

            switch ($column['DATA_TYPE']) {
                case "int":
                    $cInfo["data_type_full"] = "integer";
                    $cInfo["data_type"] = "int";
                    break;
                case "varchar":
                case "text":
                case "longtext":
                case "mediumtext":
                case "blob":
                    $cInfo["data_type_full"] = "string";
                    $cInfo["data_type"] = "string";
                    break;
                case "date":
                case "datetime":
                case "time":
                    $cInfo["data_type_full"] = "DateTime";
                    $cInfo["data_type"] = "DateTime";
                    $hasDateTime = true;
                    break;
                case "decimal":
                case "float":
                case "double":
                    $cInfo["data_type_full"] = "float";
                    $cInfo["data_type"] = "float";
                    break;
                case "tinyint":
                    $cInfo["data_type_full"] = "boolean";
                    $cInfo["data_type"] = "bool";
                    break;
                default:
                    $cInfo["data_type_full"] = "__UNKNOWN__FIX__{$column['DATA_TYPE']}";
                    $cInfo["data_type"] = "__UNKNOWN__FIX__{$column['DATA_TYPE']}";
                    break;
            }

            $columns[] = $cInfo;
        }

        $strSql = "SELECT CONSTRAINT_NAME, TABLE_NAME, REFERENCED_TABLE_NAME FROM information_schema.REFERENTIAL_CONSTRAINTS WHERE TABLE_NAME = :TABLE_NAME;";
        $arrSql = [":TABLE_NAME" => $tableName];

        foreach ($this->conn->prepare($strSql, $arrSql)->select(true) as $column) {
            $cInfo["name"] = $column["REFERENCED_TABLE_NAME"];
            $cInfo["nullable"] = true;
            $cInfo["table_link"] = true;
            $cInfo["data_type_full"] = $this->convertToCamelCase($column["REFERENCED_TABLE_NAME"]);
            $cInfo["data_type"] = $this->convertToCamelCase($column["REFERENCED_TABLE_NAME"]);
            $columns[] = $cInfo;
        }

        $entityFile = fopen("App/Models/Entities/{$this->convertToCamelCase($tableName)}.php", "w");
        fwrite($entityFile, $this->generateFileContent($this->convertToCamelCase($tableName), $columns, $hasDateTime));
        fclose($entityFile);
    }

    private function generateFileContent(string $entity, array $columns, bool $hasDateTime): string 
    {
        $content = "<?php\n\n";
        $content .= "namespace App\Models\Entities;\n\n";
        $content .= "use Core\Entity;\n";
        $content .= $hasDateTime ? "use DateTime;\n\n" : "\n";
        $content .= "class {$entity} extends Entity\n{\n";

        foreach ($columns as $column) {
            $content .= "\tprivate " . ($column["nullable"] ? "?" : "") . $column["data_type"] . " $" . 
                        $this->convertToCamelCase($column["name"], true) . ";";
            
            if ($column['table_link']) {
                $content .= " // Foreign Key Link to {$column["name"]}\n";
            } else {
                $content .= "\n";
            }
        }

        $content .= "\n";

        foreach ($columns as $column) {
            $name           = $column["name"];
            $type_full      = $column["data_type_full"];
            $type           = $column["data_type"];
            $camelCaseName  = $this->convertToCamelCase($name, true);
            $thisText       = "$" . "this";

            // Create Setter
            $content .= "\t/**\n";
            $content .= "\t * Set the $camelCaseName value\n";
            $content .= "\t *\n";
            $content .= "\t * @param $type_full" . ($column["nullable"] ? "|null" : "") . " $$camelCaseName\n";
            $content .= "\t * @return self\n";
            $content .= "\t */\n";
            $content .= "\tpublic function " . $this->convertToCamelCase("set_" . $name, true) . "(". ($column["nullable"] ? "?" : "") . "$type $$camelCaseName): self\n";
            $content .= "\t{\n";
            $content .= "\t\t$thisText->$camelCaseName = $$camelCaseName;\n";
            $content .= "\t\treturn $thisText;\n";
            $content .= "\t}\n\n";

            // Create Getter
            $content .= "\t/**\n";
            $content .= "\t * Get the $camelCaseName value\n";
            $content .= "\t *\n";
            $content .= "\t * @return $type_full\n";
            $content .= "\t */\n";
            $content .= "\tpublic function " . $this->convertToCamelCase("get_" . $name, true) . "(): " . ($column["nullable"] ? "?" : "") . "$type\n";
            $content .= "\t{\n";
            $content .= "\t\treturn $thisText->$camelCaseName;\n";
            $content .= "\t}\n\n";
        }

        $content .= "}";
        return $content;
    }
}